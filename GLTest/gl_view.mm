#import "gl_view.h"
#include "game/renderer/renderer.h"
#include <glm/glm.hpp>
#include "game/game_field.h"

static const float MAX_DELTA_TIME = 0.04f;
static const float CACHE_TIME = 10.0f;

@implementation GLView
{
}

@synthesize displayLink;
@synthesize startFrame;
@synthesize paused;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
	{
		self.startFrame = true;
		checkCacheTime = CACHE_TIME;
        [self setupLayer];        
        [self setupContext];                
        [self setupRenderBuffer];        
        [self setupFrameBuffer];
		[self beginRendering];
    }
    return self;
}

- (void)dealloc
{
    [m_GLContext release];
    m_GLContext = nil;
    [super dealloc];
}

+ (Class)layerClass
{
    return [CAEAGLLayer class];
}

- (void)setupLayer
{
	m_GLLayer = (CAEAGLLayer*) self.layer;
	m_GLLayer.opaque = YES;
}

- (void)setupContext
{
	EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES2;
	m_GLContext = [[EAGLContext alloc] initWithAPI:api];
	if (!m_GLContext)
	{
		NSLog(@"Failed to initialize OpenGLES 2.0 context");
		exit(1);
	}

	if (![EAGLContext setCurrentContext:m_GLContext])
	{
		NSLog(@"Failed to set current OpenGL context");
		exit(1);
	}
}

- (void)setupRenderBuffer
{
	glGenRenderbuffers(1, &m_ColorRenderBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, m_ColorRenderBuffer);
	[m_GLContext renderbufferStorage:GL_RENDERBUFFER fromDrawable: m_GLLayer];
}

- (void)setupFrameBuffer
{
	GLuint framebuffer;
	glGenFramebuffers(1, &framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, m_ColorRenderBuffer);
}

-(void) beginRendering
{
	displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(render)];
	[displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
}

- (void)render
{
	if (self.paused)
		return;
	glClearColor(0.2, 0.5, 0.9, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_DEPTH_BUFFER_BIT);

	CFTimeInterval curTime = displayLink.timestamp;
	CFTimeInterval deltaTime;
	deltaTime = (startFrame ? 0 : curTime - prevTime);
	startFrame = false;
	prevTime = curTime;

	if (deltaTime > MAX_DELTA_TIME)
	{
		unsigned num = deltaTime / MAX_DELTA_TIME + 1;
		float step = deltaTime / (float)num;
		while (num > 0)
		{
			GameField::instance()->update(step);
			--num;
		}
	}
	else
		GameField::instance()->update(deltaTime);
	Renderer::instance()->drawObjects(GameField::instance()->collisionSystem().objects());
	/* Checking cache. */
	checkCacheTime -= deltaTime;
	if (checkCacheTime <= 0.0f)
	{
		checkCacheTime = CACHE_TIME;
		Renderer::instance()->checkCache();
	}

	glBindRenderbuffer(GL_RENDERBUFFER, m_ColorRenderBuffer);
	[m_GLContext presentRenderbuffer:GL_RENDERBUFFER];

	GLenum err = glGetError();
	switch(err)
	{
	case GL_INVALID_ENUM:
		printf("GL_INVALID_ENUM\n");
		break;
	case GL_INVALID_VALUE:
		printf("GL_INVALID_VALUE\n");
		break;
	case GL_INVALID_OPERATION:
		printf("GL_INVALID_OPERATION\n");
		break;
	case GL_INVALID_FRAMEBUFFER_OPERATION:
		printf("GL_INVALID_FRAMEBUFFER_OPERATION\n");
		break;
	case GL_OUT_OF_MEMORY:
		printf("GL_OUT_OF_MEMORY\n");
		break;
	case GL_NO_ERROR:
//		printf("GL_NO_ERROR\n");
		break;
	default:
		printf("UNKNOWN GL ERROR\n");
		break;
	}
}

@end
