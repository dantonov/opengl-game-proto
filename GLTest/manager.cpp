#include "manager.h"
#include <istream>
#include <vector>
#include "aux/vertex.h"
#include <math.h>

static Manager * g_Instance = nullptr;

void Manager::init(const std::string & dataPath)
{
	if (g_Instance)
		return;
	g_Instance = new Manager(dataPath);
}

Manager * Manager::instance()
{
	return g_Instance;
}

Manager::Manager(const std::string & dataPath)
	: m_DataPath(dataPath)
{
	loadShaders();
	createProjectileBuffer();
}

Manager::~Manager() throw()
{
	g_Instance = nullptr;
	glDeleteBuffers(1, &m_ProjectileBuffId);
	glDeleteBuffers(1, &m_ProjectileIndexBuffId);
}

void Manager::getFileData(const std::string & fileName, char ** data, uint64_t * size)
{
	std::string filePath = m_DataPath + "/" + fileName;

	FILE * f = fopen(filePath.c_str(), "r");
	if (!f)
	{
		printf("Cannot load file: %s\n", filePath.c_str());
		return;
	}

	fseek(f, 0, SEEK_END);
	*size = ftell(f);
	*data = new char[*size];

	rewind(f);
	fread(*data, sizeof(char), (unsigned long)*size, f);
	fclose(f);
}

GLint Manager::compileShader(const std::string & fileName, GLenum shaderType)
{
	GLchar * data = nullptr;
	GLint dataSize = 0;
	getFileData(fileName, &data, (uint64_t *)&dataSize);
	if (dataSize == 0)
		return -1;

	GLuint shaderHandle = glCreateShader(shaderType);
	glShaderSource(shaderHandle, 1, &data, &dataSize);

	glCompileShader(shaderHandle);
	delete[] data;

	GLint compileSuccess;
	glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSuccess);
	if (compileSuccess == GL_FALSE)
	{
		GLchar err[256];
		glGetShaderInfoLog(shaderHandle, sizeof(err), 0, &err[0]);
		printf("Shader compile error in %s:\n%s\n", fileName.c_str(), err);
        exit(1);
    }

    return shaderHandle;
}

GLint Manager::createProgram(GLint vertexHandle, GLint fragmentHandle)
{
	GLint programHandle = glCreateProgram();
	glAttachShader(programHandle, vertexHandle);
    glAttachShader(programHandle, fragmentHandle);
    glLinkProgram(programHandle);

	GLint linkSuccess;
	glGetProgramiv(programHandle, GL_LINK_STATUS, &linkSuccess);
	if (linkSuccess == GL_FALSE)
	{
		GLchar err[256];
		glGetProgramInfoLog(programHandle, sizeof(err), 0, &err[0]);
		exit(1);
	}

	return programHandle;
}

void Manager::loadShaders()
{
	//FIXME: If one of the shader compilation is failed the another sould be unloaded.
	GLint vHandle = compileShader("common.vr", GL_VERTEX_SHADER);
	GLint fHandle = compileShader("common.fr", GL_FRAGMENT_SHADER);
	if (vHandle >= 0 && fHandle >= 0)
	{
		// This is done to simplify code at the current example code
		m_CommonProgramId = createProgram(vHandle, fHandle);
	}
}

void Manager::createProjectileBuffer()
{
	const float radius = 0.01f;
	const unsigned vertNumber = 5;
	std::vector<Vertex> vertices;
	vertices.resize(vertNumber);
	float angle = 0.0f;
	const float angleStep = 360.0f / vertNumber;
	std::for_each(vertices.begin(), vertices.end(), [this, angleStep, &angle, radius](Vertex & ver){
		ver.x = radius * cos(angle * 3.1415f / 180.0f);
		ver.y = radius * sin(angle * 3.1415f / 180.0f);

		ver.r = 0.8f;
		ver.g = 0.2f;
		ver.b = 0.2f;
		angle += angleStep;
	});

	/* Central point. */
	Vertex ver(0.0f, 0.0f, 0.4f, 0.1f, 0.1f);
	vertices.push_back(ver);

	std::vector<unsigned> indices;
	for (unsigned i = 0; i < vertNumber - 1; ++i)
	{
		indices.push_back(vertNumber);
		indices.push_back(i);
		indices.push_back(i + 1);
	}
	indices.push_back(vertNumber);
	indices.push_back(0);
	indices.push_back(vertNumber - 1);

	glGenBuffers(1, &m_ProjectileBuffId);
	glBindBuffer(GL_ARRAY_BUFFER, m_ProjectileBuffId);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &m_ProjectileIndexBuffId);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ProjectileIndexBuffId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * indices.size(), &indices[0], GL_STATIC_DRAW);
}
