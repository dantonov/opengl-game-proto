#import "gl_view_controller.h"
#include "game/game_field.h"
#include "manager.h"
#include "game/renderer/renderer.h"
#include "game/shuttle.h"

@implementation GLViewController

@synthesize glView;

-(void) dealloc
{
	self.glView = nil;
	[super dealloc];
}

-(void) viewDidLoad
{
	[super viewDidLoad];

	self.glView = [[[GLView alloc] initWithFrame:self.view.bounds] autorelease];
	[self.view addSubview:self.glView];

	NSString * dataPath = [[NSBundle mainBundle] resourcePath];
	Manager::init([dataPath UTF8String]);
	Renderer::init();

	GameField::init();

	UITapGestureRecognizer * tgr = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)] autorelease];
	[self.glView addGestureRecognizer:tgr];

	UIPanGestureRecognizer * pgr = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(onPan:)] autorelease];
	[self.glView addGestureRecognizer:pgr];
}

-(void) viewDidLayoutSubviews
{
	[super viewDidLayoutSubviews];
	[self resizeGameField];
}

-(void) resizeGameField
{
	self.glView.frame = self.view.bounds;
	float w = self.view.bounds.size.width;
	float h = self.view.bounds.size.height;
	float scale = [UIScreen mainScreen].scale;
	GameField::instance()->resize(w * scale, h * scale);
	Renderer::instance()->resize(w * scale, h * scale);
}

/* Translates coordinates from one system to another with the same coodrinate position ratio. */
-(CGPoint) transtaleCoordinates: (CGPoint) p from: (CGRect) b1 to: (CGRect) b2
{
	CGPoint coord;
	coord.x = (p.x + b1.origin.x) * b2.size.width / b1.size.width + b2.origin.x;
	coord.y = (p.y + b1.origin.y) * b2.size.height / b1.size.height + b2.origin.y;
	return coord;
}

-(float) getAngleForShuttle:(CGPoint) tapPos
{
	const glm::vec4 metrics = GameField::instance()->getFieldMetrics();
	CGPoint p = [self transtaleCoordinates:tapPos from:self.view.bounds to:CGRectMake(metrics.x, metrics.w, metrics.y - metrics.x, metrics.z - metrics.w)];
	glm::vec2 dst(p.x, p.y);

	Shuttle & shuttle = GameField::instance()->shuttle();
	glm::vec2 pos = shuttle.position();
	float ang = -shuttle.angle() * 3.1415 / 180.0f;
	float angRight = ang + 90.0f * 3.1415 / 180.0f;

	glm::vec2 dstVec = glm::normalize(dst - pos);
	glm::vec2 direction(sin(ang), cos(ang));
	glm::vec2 right(sin(angRight), cos(angRight));
	float dot = glm::dot(right, dstVec);

	float angle = acos(glm::dot(direction, dstVec));
	angle *= 180.0f / 3.1415;
	if (dot > 0.0f)
		angle = -angle;

	return angle;
}

-(float) getAngle:(CGPoint) vel
{
	glm::vec2 pos(0.0f, 0.0f);
	float ang = -GameField::instance()->shuttle().angle() * 3.1415 / 180.0f;
	float angRight = ang + 90.0f * 3.1415 / 180.0f;

	glm::vec2 dst(vel.x, vel.y);
	glm::vec2 dstVec = glm::normalize(dst - pos);
	glm::vec2 direction(sin(ang), cos(ang));
	glm::vec2 right(sin(angRight), cos(angRight));
	float dot = glm::dot(right, dstVec);

	float angle = acos(glm::dot(direction, dstVec));
	angle *= 180.0f / 3.1415;
	if (dot > 0.0f)
		angle = -angle;

	return angle;
}

-(void) onTap: (UITapGestureRecognizer *) tgr
{
	if (!GameField::instance()->gameIsOver())
	{
		CGPoint p = [tgr locationInView:self.glView];

		float angle = [self getAngleForShuttle:p];
		Shuttle & shuttle = GameField::instance()->shuttle();
		shuttle.setFinalAngle(angle);
	}
	else
	{
		GameField::reinit();
		[self resizeGameField];
	}
}

-(void) onPan: (UIPanGestureRecognizer *) pgr
{
	if (GameField::instance()->gameIsOver())
		return;
	Shuttle & shuttle = GameField::instance()->shuttle();
	CGPoint v = [pgr velocityInView: self.glView];
	CGPoint speed;
	speed.x = shuttle.speed().x;
	speed.y = shuttle.speed().y;
	if (speed.x != 0.0f && speed.y != 0.0f)
	{
		float angle = [self getAngle: speed];
		shuttle.setFinalAngle(angle);
	}
	shuttle.setAcceleration(v.x * 0.01f, v.y * 0.01f);
}

@end
