
Game code structure is designed like MVC, but not pure MVC.
Model - collision system with objects.
View - renderer.
Controller - is a small system which contains in Model and as gesture detector controls. (That because it is not pure MVC.)

This small prototype is created in few days, so there is some small bugs and code which needsto be optimized.
So, separated renderer needs some system to cache vertex buffer objects, I'm using CRC for this, also cache need to
be freed after some useage time, I've set it to 10 sec, but of course it bigger projects it is better to free some cache while
levels are changeing or in moments when framerate may not be as high as posssible.
