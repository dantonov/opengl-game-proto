
#import <UIKit/UIKit.h>

@class GLViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) GLViewController * glViewController;

@end
