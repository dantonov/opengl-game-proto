
#import "AppDelegate.h"
#import "gl_view_controller.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

	self.window.backgroundColor = [UIColor whiteColor];
	[self.window makeKeyAndVisible];

	UIViewController * vc = [[[UIViewController alloc] init] autorelease];
	[self.window setRootViewController: vc];

	self.glViewController = [[[GLViewController alloc] init] autorelease];
	[vc presentViewController:self.glViewController animated:NO completion:nil];

	return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	self.glViewController.glView.paused = YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	self.glViewController.glView.paused = YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	self.glViewController.glView.paused = NO;
	self.glViewController.glView.startFrame = true;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	self.glViewController.glView.paused = NO;
	self.glViewController.glView.startFrame = true;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

@end
