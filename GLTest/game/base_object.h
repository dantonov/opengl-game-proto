#ifndef __25a302be28756f34afc3ffaf3f2904b6__
#define __25a302be28756f34afc3ffaf3f2904b6__

#include "../aux/vertex.h"
#include "../aux/simple_math.h"
#include <vector>
#include <glm/glm.hpp>
#include <assert.h>

class BaseObject;

typedef std::shared_ptr<BaseObject> BaseObjectPtr;

class BaseObject
{
public:
	inline BaseObject(unsigned typeId) : m_Angle(0), m_IsRemoved(false), m_TypeId(typeId), m_CRC(0) {}
	inline virtual ~BaseObject() throw() {}

	BaseObject & operator=(const BaseObject &) = delete;

	inline float angle() const { return m_Angle; }
	inline void setAngle(float a) { m_Angle = a; }
	inline const glm::vec2 & position() const { return m_Position; }
	inline void setPosition(const glm::vec2 & pos) { m_Position = pos; }
	inline void remove() { m_IsRemoved = true; }
	inline bool isRemoved() const { return m_IsRemoved; }
	inline const std::vector<Vertex> & vertices() const { return m_Vertices; }
	inline const std::vector<unsigned> & indices() const { return m_Indices; }
	inline unsigned typeId() const { return m_TypeId; }
	inline uint16_t crc() const { assert(m_CRC > 0); return m_CRC; }

protected:
	float m_Angle;
	glm::vec2 m_Position;
	std::vector<Vertex> m_Vertices;
	std::vector<unsigned> m_Indices;

	inline void calculateCRC() {
		unsigned size = m_Vertices.size() * sizeof(Vertex);
		const uint8_t * data = reinterpret_cast<const uint8_t* >(&m_Vertices[0]);
		m_CRC = gen_crc16(data, size);
	}

private:
	bool m_IsRemoved;
	unsigned m_TypeId;
	uint16_t m_CRC;
};

#endif
