#ifndef __2022236e3c1b17b28619729dc2ac9626__
#define __2022236e3c1b17b28619729dc2ac9626__

#include "model/collision_object.h"

class GameField;

class Shuttle : public CollisionObject
{
public:
	constexpr static const float FIRE_RATE = 0.3f;

	Shuttle();
	~Shuttle() throw();

	void setFinalAngle(float a);
	inline float finalAngle() const { return m_AngleFinal; }
	inline void setAcceleration(float x, float y) { m_Acceleration.x = x; m_Acceleration.y = y; }

	void onCollisionDetected(const CollisionObject * obj) override;

private:
	float m_AngleCurrent;
	float m_AngleFinal;
	float m_AngleSpeed;
	glm::vec2 m_Acceleration;

	float m_TimeToFire;

friend class GameField;
};

#endif
