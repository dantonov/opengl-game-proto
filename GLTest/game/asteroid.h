#ifndef __48d22d242ab1d9dc41a7f81ce2aaea5d__
#define __48d22d242ab1d9dc41a7f81ce2aaea5d__

#include "model/collision_object.h"

class Asteroid : public CollisionObject
{
public:
	Asteroid();
	Asteroid(const std::vector<Vertex> & vertices);
	~Asteroid() throw();

	void onCollisionDetected(const CollisionObject * obj) override;
};

#endif
