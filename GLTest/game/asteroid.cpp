#include "asteroid.h"
#include "object_ids.h"

Asteroid::Asteroid()
	: CollisionObject(ASTEROID)
{
	m_Radius = (rand() % 50 + 100) * 0.001f;

	unsigned vertNumber = rand() % 3 + 6;
	m_Vertices.resize(vertNumber);
	float angleStep = 360.0f / (vertNumber);
	float angle = 0.0f;
	std::for_each(m_Vertices.begin(), m_Vertices.end(), [this, &angleStep, &angle](Vertex & ver){
		ver.x = (m_Radius - 0.001f * (rand() % 20)) * cos(angle * 3.1415f / 180.0f);
		ver.y = (m_Radius - 0.001f * (rand() % 20)) * sin(angle * 3.1415f / 180.0f);

		ver.r = 0.8f;
		ver.g = 0.8f;
		ver.b = 0.3f;
		angle += angleStep;
	});

	/* Central point. */
	m_Vertices.push_back(Vertex(0.0f, 0.0f, 0.8f, 0.8f, 0.8f));
	unsigned size = m_Vertices.size();
	for (unsigned i = 0; i < size - 2; ++i)
	{
		m_Indices.push_back(m_Vertices.size() - 1);
		m_Indices.push_back(i);
		m_Indices.push_back(i + 1);
	}
	m_Indices.push_back(size - 2);
	m_Indices.push_back(0);
	m_Indices.push_back(size - 1);

	calculateCRC();
}

Asteroid::Asteroid(const std::vector<Vertex> & vertices)
	: CollisionObject(ASTEROID)
{
	m_Vertices = vertices;
	glm::vec3 color;
	float maxX = 0.0f;
	float maxY = 0.0f;
	std::for_each(m_Vertices.begin(), m_Vertices.end(), [&color, &maxX, &maxY](Vertex & ver)
	{
		if (maxX < fabs(ver.x))
			maxX = ver.x;
		if (maxY < fabs(ver.y))
			maxY = ver.y;

		color.r += ver.r;
		color.g += ver.g;
		color.b += ver.b;
	});
	m_Radius = sqrt(maxX * maxX + maxY * maxY) + 0.001f;
	color /= m_Vertices.size();
	/* Central point. */
	Vertex ver(0.0f, 0.0f, color.r, color.g, color.b);
	m_Vertices.push_back(ver);

	for (unsigned i = 0; i < m_Vertices.size() - 2; ++i)
	{
		m_Indices.push_back(m_Vertices.size() - 1);
		m_Indices.push_back(i);
		m_Indices.push_back(i + 1);
	}
	m_Indices.push_back(m_Vertices.size() - 1);
	m_Indices.push_back(0);
	m_Indices.push_back(m_Vertices.size() - 2);

	calculateCRC();
}

Asteroid::~Asteroid() throw()
{
}

void Asteroid::onCollisionDetected(const CollisionObject * obj)
{
//	printf("ASTEROID COLLISION DETECTED\n");
}
