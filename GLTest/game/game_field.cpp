#include "game_field.h"
#include "object_ids.h"
#include "shuttle.h"
#include "asteroid.h"
#include "projectile.h"

static GameField * g_Instance = nullptr;
static const unsigned MAX_ASTEROIDS = 10;
static const float BORDER = 0.15f;
static glm::vec4 g_Borders;

void GameField::init()
{
	if (g_Instance)
		return;

	g_Instance = new GameField();
}

void GameField::reinit()
{
	if (g_Instance)
		delete g_Instance;

	g_Instance = new GameField();
}

GameField * GameField::instance()
{
	return g_Instance;
}

GameField::GameField()
	: BaseObject(GAMEFIELD),
	  m_GameIsOver(false)
{
	m_CollisionSystem.setDelegate(this);
	m_Shuttle.reset(new Shuttle());
	m_CollisionSystem.addObject(m_Shuttle);
}

GameField::~GameField() throw()
{
}

void GameField::resize(float w, float h)
{
	m_Size.x = w;
	m_Size.y = h;
//	if (w < h)
//		glViewport(0, 0, m_Size.x, m_Size.y);
//	else
//		glViewport(0, 0, m_Size.y, m_Size.x);

	getFieldMetrics();
	m_Resized = true;
}

glm::vec4 GameField::getFieldMetrics() const
{
	float left = -1.0;
	float right = 1.0;
	float bottom = 1.0;
	float top = -1.0;
	float sideRatio = 1.0;
	if (m_Size.x < m_Size.y)
	{
		sideRatio = m_Size.x / m_Size.y;
		left = -sideRatio;
		right = sideRatio;
	}
	else
	{
		sideRatio = m_Size.y / m_Size.x;
		top = -sideRatio;
		bottom = sideRatio;
	}

	m_Metrics = glm::vec4(left, right, bottom, top);
	g_Borders = m_Metrics;
	g_Borders.x -= BORDER;
	g_Borders.y += BORDER;
	g_Borders.w -= BORDER;
	g_Borders.z += BORDER;

	return m_Metrics;
}

void GameField::emitProjectile()
{
	const float correction = 90.0f * 3.1415 / 180.0f; // Because zero angle looks at axis Y not X
	float angle = m_Shuttle->angle() * 3.1415 / 180.0f + correction;
	glm::vec2 vec(cos(angle), sin(angle));
	glm::vec2 pos(m_Shuttle->position());

	emitProjectile(pos, vec);
}

void GameField::emitProjectile(const glm::vec2 & pos, const glm::vec2 & direction)
{
	BaseObjectPtr proj;
	proj.reset(new Projectile());
	proj->setPosition(pos);
	static_cast<CollisionObject *>(proj.get())->setSpeed(direction);
	m_CollisionSystem.addObject(proj);
}

void GameField::update(float deltaTime)
{
	m_CollisionSystem.update(deltaTime);

	if (!m_Resized)
		return;

	while (m_CollisionSystem.getNumberOfObjects(ASTEROID) < MAX_ASTEROIDS)
		emitAsteroid();

	updateShuttle(deltaTime);
	glm::vec4 metrics = g_Borders;
	glm::vec2 sPos = m_Shuttle->position();
	if (sPos.x < metrics.x)//left
		sPos.x = metrics.y + (sPos.x - metrics.x);
	else if (sPos.y < metrics.w)//top
		sPos.y = metrics.z + (sPos.y - metrics.w);
	if (sPos.x > metrics.y)//right
		sPos.x = metrics.x + (sPos.x - metrics.y);
	else if (sPos.y > metrics.z)//bottom
		sPos.y = metrics.w + (sPos.y - metrics.z);
	m_Shuttle->setPosition(sPos);
}

bool GameField::isOutOfBorders(const BaseObject * bo)
{
	if (bo->typeId() == SHUTTLE)
		return false;

	const glm::vec2 & pos = bo->position();
	glm::vec4 metrics = g_Borders;
	/* x=Left, y=Right, z=Bottom, w=Top. */
	if (pos.x < metrics.x || pos.x > metrics.y || pos.y < metrics.w || pos.y > metrics.z)
		return true;
	return false;
}

void GameField::emitAsteroid()
{
	unsigned rnd = rand() % 4;
	float sidePos = (rand() % 100) * 0.01;
	float rndSpeed1 = (rand() % 10 + 10) * 0.01f;
	float rndSpeed2 = (rand() % 10 - 5) * 0.01f;

	/* Right - Left. */
	float w = g_Borders.y - g_Borders.x;
	/* Top - Bottom. */
	float h = g_Borders.w - g_Borders.z;

	glm::vec2 pos;
	glm::vec2 speed;
	switch (rnd)
	{
	case 0: /* Left side. */
		pos.x = g_Borders.x;
		pos.y = sidePos * h + g_Borders.z;
		speed.x = rndSpeed1;
		speed.y = rndSpeed2;
		break;
	case 1: /* Top side. */
		pos.x = sidePos * w + g_Borders.x;
		pos.y = g_Borders.w;
		speed.x = rndSpeed2;
		speed.y = rndSpeed1;
		break;
	case 2: /* Right side. */
		pos.x = g_Borders.y;
		pos.y = sidePos * h + g_Borders.z;
		speed.x = -rndSpeed1;
		speed.y = rndSpeed2;
		break;
	case 3: /* Bottom side. */
		pos.x = sidePos * w + g_Borders.x;
		pos.y = g_Borders.z;
		speed.x = rndSpeed2;
		speed.y = -rndSpeed1;
		break;
	}

	BaseObjectPtr ast;
	ast.reset(new Asteroid());
	ast->setPosition(pos);
	static_cast<CollisionObject *>(ast.get())->setSpeed(speed);
	m_CollisionSystem.addObject(ast);
}

void GameField::updateShuttle(float deltaTime)
{
	if (m_Shuttle->isRemoved())
		return;

	float & timeToFire = m_Shuttle->m_TimeToFire;
	timeToFire -= deltaTime;
	if (timeToFire <= 0.0f)
	{
		emitProjectile();
		timeToFire = Shuttle::FIRE_RATE;
	}

	glm::vec2 & acceleration = m_Shuttle->m_Acceleration;
	glm::vec2 & speed = m_Shuttle->speed();
	if (acceleration.x == 0.0f && acceleration.y == 0.0f)
		speed *= 0.99f;
	else
	{
		speed += acceleration * deltaTime;
		acceleration *= 0.0f;
	}

	float & angleFinal = m_Shuttle->m_AngleFinal;
	float & angleCurrent = m_Shuttle->m_AngleCurrent;
	if (angleFinal == angleCurrent)
		return;

	float absAngleDelta = fabs(angleFinal - angleCurrent);
	float angleDelta = m_Shuttle->m_AngleSpeed * deltaTime * 10.0f; // *10.0f - to make if faster.
	float angle = m_Shuttle->angle();
	if (absAngleDelta <= fabs(angleDelta))
	{
		angleCurrent = angleFinal;
		angle += absAngleDelta;
	}
	else
	{
		angleCurrent+= angleDelta;
		angle += angleDelta;
	}

	if (angle < 0.0f)
		angle = 360.0f - angle;
	else if (angle > 360.0f)
		angle = angle - 360.0;
	m_Shuttle->setAngle(angle);
}

void GameField::collide(CollisionObject * projectile, CollisionObject * asteroid)
{
	projectile->remove();
	asteroid->remove();

	const std::vector<Vertex> & vertices = asteroid->vertices();
	const std::vector<unsigned> & indices = asteroid->indices();

	if (vertices.size() < 5)
		return;

	std::vector<Vertex> newVerts;
	newVerts.resize(3);
	for (unsigned i = 0; i < indices.size() - 1; i += 3)
	{
		newVerts[0] = vertices[indices[i]];
		newVerts[1] = vertices[indices[i + 1]];
		newVerts[2] = vertices[indices[i + 2]];
		glm::vec2 center;
		for (unsigned j = 0; j < 3; ++j)
		{
			center.x += newVerts[j].x;
			center.y += newVerts[j].y;
		}
		center *= 0.333333f;

		BaseObjectPtr ast;
		ast.reset(new Asteroid(newVerts));
		ast->setPosition(asteroid->position());
		static_cast<CollisionObject *>(ast.get())->setSpeed(center);
		m_CollisionSystem.addObject(ast);
	}
}
