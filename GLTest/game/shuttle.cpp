#include "shuttle.h"
#include "game_field.h"
#include "object_ids.h"

Shuttle::Shuttle()
	: CollisionObject(static_cast<unsigned>(SHUTTLE))
{
	m_AngleCurrent = 0.0f;
	m_AngleFinal = 0.0f;
	m_AngleSpeed = 0.0f;
	m_TimeToFire = FIRE_RATE;

	// All of this is translated from Blender as is, so the model is hardcoded.
	m_Vertices = {{0.5, -1.0,	/*0 color*/ 0.3, 0.8, 0.3},
				{-0.5, -1.0,	/*1 color*/ 0.3, 0.8, 0.3},
				{1.0, -0.5,		/*2 color*/ 0.3, 0.8, 0.3},
				{0.19, 1.0,		/*3 color*/ 0.3, 0.8, 0.3},
				{-0.19, 1.0,	/*4 color*/ 0.3, 0.8, 0.3},
				{-1.0, -0.5,	/*5 color*/ 0.3, 0.8, 0.3},
				{-1.0, 0.0,		/*6 color*/ 0.3, 0.8, 0.3},
				{1.0, 0.0,		/*7 color*/ 0.3, 0.8, 0.3}};
	std::for_each(m_Vertices.begin(), m_Vertices.end(), [this](Vertex & ver)
	{
		ver.x *= 0.075f;
		ver.y *= 0.075f;
		m_Radius = std::max(m_Radius, (float)sqrt(ver.x * ver.x + ver.y * ver.y));
	});

	m_Indices = {3, 4, 6,
				3, 6, 7,
				6, 7, 5,
				7, 5, 2,
				6, 5, 2,
				5, 1, 2,
				2, 1, 0};

	calculateCRC();
}

Shuttle::~Shuttle() throw()
{
}

void Shuttle::setFinalAngle(float a)
{
	m_AngleFinal = a;
	m_AngleCurrent = 0.0f;
	float angleDelta = m_AngleFinal - m_AngleCurrent;
	if (angleDelta <= 180.0f)
		m_AngleSpeed = angleDelta / fabs(angleDelta) * 50;
	else
		m_AngleSpeed = -angleDelta / fabs(angleDelta) * 50;
}

void Shuttle::onCollisionDetected(const CollisionObject * obj)
{
	if (obj->typeId() == ASTEROID)
	{
		remove();
		GameField::instance()->setGameIsOver();
	}
}
