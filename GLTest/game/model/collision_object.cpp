#include "collision_object.h"

CollisionObject::CollisionObject(unsigned objTypeId)
	: BaseObject(objTypeId),
	  m_Radius(0)
{
}

CollisionObject::CollisionObject(const std::vector<Vertex> & vertices, unsigned objTypeId)
	: BaseObject(objTypeId),
	  m_Radius(0)
{
	m_Vertices = vertices;
	calculateCRC();
}

CollisionObject::~CollisionObject() throw()
{
}

void CollisionObject::recalcNextGlobalVertexPositions()
{
	m_NextGlobalVertexPositions.resize(m_Vertices.size());
	unsigned index = 0;
	std::for_each(m_Vertices.begin(), m_Vertices.end(), [this, &index](Vertex & ver){
		Vertex & v = m_NextGlobalVertexPositions[index];
		v.x = ver.x + m_NextPosition.x;
		v.y = ver.y + m_NextPosition.y;
		++index;
	});
}
