#ifndef __693e12e68452cf8a63ccdab77fce26b0__
#define __693e12e68452cf8a63ccdab77fce26b0__

#include "../base_object.h"
#include "../../aux/vertex.h"
#include <vector>
#include <set>
#include <glm/glm.hpp>

class CollisionObject : public BaseObject
{
public:
	CollisionObject(unsigned objTypeId);
	CollisionObject(const std::vector<Vertex> & vertices, unsigned objTypeId);
	~CollisionObject() throw();
	CollisionObject & operator=(const CollisionObject &) = delete;

	inline float radius() const { return m_Radius; }
	inline const glm::vec2 & nextPosition() const { return m_NextPosition; }
	inline void setNextPosition(const glm::vec2 & pos) { m_NextPosition = pos; }
	inline glm::vec2 & speed() { return m_Speed; }
	inline void setSpeed(const glm::vec2 & speed) { m_Speed = speed; }
	inline const std::vector<Vertex> & vertices() const { return m_Vertices; }

	inline void insertCollisionCacheObject(const CollisionObject * obj) { m_CollisionCache.insert(obj); }
	inline bool collisionCacheContains(const CollisionObject * obj) const { return m_CollisionCache.find(obj) != m_CollisionCache.end(); }
	inline void clearCollisionCache() { m_CollisionCache.clear(); }

	void recalcNextGlobalVertexPositions();
	inline const std::vector<Vertex> nextGlobalVertexPositions() const { return m_NextGlobalVertexPositions; }

	virtual void onCollisionDetected(const CollisionObject * obj) = 0;

protected:
	float m_Radius;
	glm::vec2 m_NextPosition;
	glm::vec2 m_Speed;

private:
	std::vector<Vertex> m_NextGlobalVertexPositions;
	std::set<const CollisionObject *> m_CollisionCache;
};

#endif
