#ifndef __9e2a3e30d05eb7c636a561de47001425__
#define __9e2a3e30d05eb7c636a561de47001425__

#include "../base_object.h"
#include <vector>
#include <unordered_map>

class CollisionObject;
struct Vertex;

class CollisionSystemDelegate
{
public:
	virtual bool isOutOfBorders(const BaseObject * bo) = 0;
};

class CollisionSystem
{
public:
	CollisionSystem();
	~CollisionSystem() throw();

	inline const std::vector<BaseObjectPtr> & objects() const { return m_Objects; }

	void addObject(BaseObjectPtr obj);
	void removeObject(BaseObjectPtr obj);
	unsigned getNumberOfObjects(unsigned typeId);
	void update(float deltaTime);
	inline void setDelegate(CollisionSystemDelegate * d) { m_Delegate = d; }

private:
	CollisionSystemDelegate * m_Delegate;
	std::vector<BaseObjectPtr> m_Objects;
	std::vector<BaseObjectPtr> m_ObjectsToAdd;
	std::unordered_map<unsigned, unsigned> m_ObjectsStatistic; // Containd object ID - number of objects pairs.

	void prepareNextFrame(float deltaTime);
	void commitNextFrame();
	void updateObject(CollisionObject * obj, float deltaTime);
	void processCollisions(CollisionObject * cObj1, CollisionObject * cObj2);
	bool checkCollisionsForEdge(float x, float y, const std::vector<Vertex> & vertices, const std::vector<unsigned> & indices);
	bool checkCollisionsForEdge(float x1, float y1, float x2, float y2, const std::vector<Vertex> & vertices);
	void onObjectRemoved(BaseObject * co);
	void moveObjectsToSystem();
};

#endif
