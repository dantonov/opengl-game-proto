#include "collision_system.h"
#include "collision_object.h"
#include "../../aux/simple_math.h"

CollisionSystem::CollisionSystem()
	: m_Delegate(nullptr)
{
}

CollisionSystem::~CollisionSystem() throw()
{
}

void CollisionSystem::addObject(BaseObjectPtr obj)
{
	std::unordered_map<unsigned, unsigned>::iterator it = m_ObjectsStatistic.find(obj->typeId());
	if (it != m_ObjectsStatistic.end())
		(*it).second += 1;
	else
		m_ObjectsStatistic.insert(std::make_pair(obj->typeId(), 1));

	m_ObjectsToAdd.push_back(obj);
}

void CollisionSystem::moveObjectsToSystem()
{
	while (m_ObjectsToAdd.size() > 0)
	{
		m_Objects.push_back(m_ObjectsToAdd.back());
		m_ObjectsToAdd.pop_back();
	}
}

void CollisionSystem::removeObject(BaseObjectPtr obj)
{
	std::vector<BaseObjectPtr>::iterator it =m_Objects.begin();
	while (it != m_Objects.end() || (*it).get() != obj.get())
		++it;
	if (it != m_Objects.end())
		m_Objects.erase(it);

	onObjectRemoved(obj.get());
}

unsigned CollisionSystem::getNumberOfObjects(unsigned typeId)
{
	std::unordered_map<unsigned, unsigned>::iterator it = m_ObjectsStatistic.find(typeId);
	return it != m_ObjectsStatistic.end() ? (*it).second : 0;
}

void CollisionSystem::update(float deltaTime)
{
	prepareNextFrame(deltaTime);

	for (std::vector<BaseObjectPtr>::iterator it = m_Objects.begin(); it != m_Objects.end();)
	{
		if ((*it)->isRemoved())
		{
			onObjectRemoved(it->get());
			it = m_Objects.erase(it);
			continue;
		}
		else
		{
			CollisionObject * obj = static_cast<CollisionObject *>((*it).get());
			updateObject(obj, deltaTime);
			if (m_Delegate && m_Delegate->isOutOfBorders(obj))
				(*it)->remove();
			++it;
		}
	}

	commitNextFrame();
	moveObjectsToSystem();
}

void CollisionSystem::prepareNextFrame(float deltaTime)
{
	for (std::vector<BaseObjectPtr>::iterator it = m_Objects.begin(); it != m_Objects.end(); ++it)
	{
		if ((*it)->isRemoved())
			continue;
		else
		{
			CollisionObject * obj = static_cast<CollisionObject *>((*it).get());
			glm::vec2 step = obj->speed() * deltaTime;
			obj->setNextPosition(obj->position() + step);
			obj->recalcNextGlobalVertexPositions();
			obj->clearCollisionCache();
		}
	}
}

void CollisionSystem::commitNextFrame()
{
	for (std::vector<BaseObjectPtr>::iterator it = m_Objects.begin(); it != m_Objects.end(); ++it)
	{
		if ((*it)->isRemoved())
			continue;
		else
		{
			CollisionObject * obj = static_cast<CollisionObject *>((*it).get());
			obj->setPosition(obj->nextPosition());
		}
	}
}

void CollisionSystem::updateObject(CollisionObject * obj, float deltaTime)
{
	for (std::vector<BaseObjectPtr>::iterator it = m_Objects.begin(); it != m_Objects.end(); ++it)
	{
		if ((*it)->isRemoved() || (*it).get() == obj)
			continue;
		else
		{
			CollisionObject * cObj = static_cast<CollisionObject *>((*it).get());
			glm::vec2 delta = obj->position() - cObj->position();
			if (length(delta) < obj->radius() + cObj->radius() && !cObj->isRemoved())
				processCollisions(obj, cObj);
		}
	}
}

void CollisionSystem::processCollisions(CollisionObject * cObj1, CollisionObject * cObj2)
{
	if (cObj1->collisionCacheContains(cObj2))
	{
		cObj1->onCollisionDetected(cObj2); // Calling back only first(main) object, the second will be called when it becomes the main.
		return;
	}

	const std::vector<Vertex> & obj1Vertices = cObj1->nextGlobalVertexPositions();
	const std::vector<Vertex> & obj2Vertices = cObj2->nextGlobalVertexPositions();
	const std::vector<unsigned> & obj2Indices = cObj2->indices();

	for (unsigned i = 0; i < obj1Vertices.size() - 1; ++i)
	{
		const Vertex & v1 = obj1Vertices[i];

		if (!checkCollisionsForEdge(v1.x, v1.y, obj2Vertices, obj2Indices))
			continue;
		else
		{
			cObj1->insertCollisionCacheObject(cObj2);
			cObj2->insertCollisionCacheObject(cObj1);
			cObj1->onCollisionDetected(cObj2); // Calling back only first(main) object, the second will be called when it becomes the main.
			return;
		}
	}
}

bool CollisionSystem::checkCollisionsForEdge(float x, float y, const std::vector<Vertex> & vertices, const std::vector<unsigned> & indices)
{
	for (unsigned i = 0; i < indices.size(); i += 3)
	{
		const Vertex & v0 = vertices[indices[i]];
		const Vertex & v1 = vertices[indices[i + 1]];
		const Vertex & v2 = vertices[indices[i + 2]];
		if (triangleContainsPoint(v0.x, v0.y, v1.x, v1.y, v2.x, v2.y, x, y))
			return true;
	}

	return false;
}

bool CollisionSystem::checkCollisionsForEdge(float x1, float y1, float x2, float y2, const std::vector<Vertex> & vertices)
{
	unsigned i = 0;

	for (std::vector<Vertex>::const_iterator cit = vertices.begin(); cit != vertices.end(); ++cit)
	{
		const Vertex & v1 = vertices[i];
		unsigned j = i + 1;
		if (j == vertices.size() - 1)
			j = 0;
		const Vertex & v2 = vertices[j];

		if (IsLinesCross(v1.x, v1.y, v2.x, v2.x, x1, y1, x2, y2))
			return true;
		++i;
	}

	return false;
}

void CollisionSystem::onObjectRemoved(BaseObject * co)
{
	if (!co)
		return;
	std::unordered_map<unsigned, unsigned>::iterator it = m_ObjectsStatistic.find(co->typeId());
	if (it == m_ObjectsStatistic.end())
		printf("ERROR: THE OBJECT IS NOT CONTAINED IN STATISTIC\n");
	else
		--(it->second);
}
