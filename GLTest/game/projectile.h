#ifndef __f6867f7e9d2c28dddbf0f389cc340901__
#define __f6867f7e9d2c28dddbf0f389cc340901__

#include "model/collision_object.h"

class Projectile : public CollisionObject
{
public:
	Projectile();
	~Projectile() throw();

	void onCollisionDetected(const CollisionObject * obj) override;
};

#endif
