#include "projectile.h"
#include "object_ids.h"
#include "game_field.h"

Projectile::Projectile()
	: CollisionObject(PROJECTILE)
{
	const float radius = 0.01f;
	const unsigned vertNumber = 4;
	m_Vertices.resize(vertNumber);
	float angle = 0.0f;
	const float angleStep = 360.0f / vertNumber;
	std::for_each(m_Vertices.begin(), m_Vertices.end(), [this, angleStep, &angle, radius](Vertex & ver){
		ver.x = radius * cos(angle * 3.1415f / 180.0f);
		ver.y = radius * sin(angle * 3.1415f / 180.0f);

		ver.r = 0.8f;
		ver.g = 0.2f;
		ver.b = 0.2f;
		angle += angleStep;
	});

	/* Central point. */
	Vertex ver(0.0f, 0.0f, 0.4f, 0.1f, 0.1f);
	m_Vertices.push_back(ver);

	for (unsigned i = 0; i < vertNumber - 1; ++i)
	{
		m_Indices.push_back(vertNumber);
		m_Indices.push_back(i);
		m_Indices.push_back(i + 1);
	}
	m_Indices.push_back(vertNumber);
	m_Indices.push_back(0);
	m_Indices.push_back(vertNumber - 1);

	calculateCRC();
}

Projectile::~Projectile() throw()
{
}

void Projectile::onCollisionDetected(const CollisionObject * obj)
{
	if (obj->typeId() == ASTEROID && !isRemoved() && !obj->isRemoved())
		GameField::instance()->collide(this, const_cast<CollisionObject *>(obj));
}
