#ifndef __da76114e4f77db0c29e5db711d7bb45e__
#define __da76114e4f77db0c29e5db711d7bb45e__

enum ObjId
{
	GAMEFIELD = 1,
	SHUTTLE = 2,
	PROJECTILE = 3,
	ASTEROID = 4
};

#endif
