#ifndef __7d08e218dc903932ef19d7151db76589__
#define __7d08e218dc903932ef19d7151db76589__

#include "model/collision_system.h"
#include "base_object.h"
#include <glm/glm.hpp>

class Shuttle;

class GameField : public BaseObject, protected CollisionSystemDelegate
{
public:
	static void init();
	static void reinit();
	static GameField * instance();

	GameField();
	~GameField() throw();

	void resize(float w, float h);
	glm::vec4 getFieldMetrics() const;
	inline glm::vec4 metrics() const { return m_Metrics; }
	void emitProjectile();
	void emitProjectile(const glm::vec2 & pos, const glm::vec2 & direction);
	void update(float deltaTime);
	inline const CollisionSystem & collisionSystem() const { return m_CollisionSystem; }
	inline Shuttle & shuttle() { return *(m_Shuttle.get()); }
	inline bool gameIsOver() { return m_GameIsOver; }
	inline void setGameIsOver() { m_GameIsOver = true; }

	void collide(CollisionObject * projectile, CollisionObject * asteroid);

protected:
	bool isOutOfBorders(const BaseObject * bo);

private:
	CollisionSystem m_CollisionSystem;
	std::shared_ptr<Shuttle> m_Shuttle;
	std::vector<CollisionObject *> m_ObjectsToAdd;

	bool m_Resized;
	glm::vec2 m_Size;
	mutable glm::vec4 m_Metrics;
	bool m_GameIsOver;

	void emitAsteroid();
	void updateShuttle(float deltaTime);
};

#endif
