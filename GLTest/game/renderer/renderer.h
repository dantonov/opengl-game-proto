#ifndef __d19be8b188432d349e1ed2a2ab8f59d8__
#define __d19be8b188432d349e1ed2a2ab8f59d8__

#include <map>
#include <vector>
#include <glm/glm.hpp>
#include "../base_object.h"
#include <OpenGLES/ES2/gl.h>

class Renderer
{
public:
	static void init();
	static Renderer * instance();

	Renderer();
	~Renderer() throw();

	void checkCache();
	void update(float time);
	void drawObjects(const std::vector<BaseObjectPtr> & objects);
	void resize(float w, float h);
	glm::vec4 getFieldMetrics() const;
	inline glm::vec4 metrics() const { return m_Metrics; }

private:
	struct BufferInfo
	{
		float usageTime;
		unsigned bufferId;
		unsigned indexBufferId;

		BufferInfo(float uT, unsigned bI, unsigned iBI) : usageTime(uT), bufferId(bI), indexBufferId(iBI) {}
	};

	std::map<uint16_t, BufferInfo> m_ObjectCache;
	float m_CurrentTime;
	float m_LastCheckTime;

	bool m_Resized;
	glm::vec2 m_Size;
	mutable glm::vec4 m_Metrics;

	void drawObject(const BaseObject * bo, const glm::mat4 & viewMx);
	void loadObject(const BaseObject * bo, GLuint * bufferId, GLuint * indexBufferId);
};

#endif
