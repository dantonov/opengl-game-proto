#include "renderer.h"
#include "../../aux/simple_math.h"
#include "../../manager.h"
#include <glm/gtc/matrix_transform.hpp>

static Renderer * g_Renderer = nullptr;
static const float CACHE_TIME = 1.0f;

void Renderer::init()
{
	if (!g_Renderer)
		g_Renderer = new Renderer();
}

Renderer * Renderer::instance()
{
	return g_Renderer;
}

Renderer::Renderer()
	: m_CurrentTime(0),
	  m_LastCheckTime(0)
{
}

Renderer::~Renderer() throw()
{
	/** Free all used buffers. */
}

void Renderer::checkCache()
{
	for (std::map<uint16_t, BufferInfo>::iterator it = m_ObjectCache.begin(); it != m_ObjectCache.end();)
	{
		BufferInfo & info = it->second;
		if (m_CurrentTime - info.usageTime < CACHE_TIME)
			++it;
		else
		{
			glDeleteBuffers(1, &info.bufferId);
			glDeleteBuffers(1, &info.indexBufferId);
			it = m_ObjectCache.erase(it);
		}
	}
}

void Renderer::update(float deltaTime)
{
	m_CurrentTime += deltaTime;
}

void Renderer::drawObjects(const std::vector<BaseObjectPtr> & objects)
{
	glm::mat4 viewMx;
	glm::vec4 metrics = getFieldMetrics();
	viewMx = glm::ortho(metrics.x, metrics.y, metrics.z, metrics.w, -1.0f, 1.0f);

	for (std::vector<BaseObjectPtr>::const_iterator it = objects.begin(); it != objects.end(); ++it)
		drawObject((*it).get(), viewMx);
}

void Renderer::resize(float w, float h)
{
	m_Size.x = w;
	m_Size.y = h;
	if (w < h)
		glViewport(0, 0, m_Size.x, m_Size.y);
	else
		glViewport(0, 0, m_Size.y, m_Size.x);

	getFieldMetrics();
	m_Resized = true;
}

glm::vec4 Renderer::getFieldMetrics() const
{
	float left = -1.0;
	float right = 1.0;
	float bottom = 1.0;
	float top = -1.0;
	float sideRatio = 1.0;
	if (m_Size.x < m_Size.y)
	{
		sideRatio = m_Size.x / m_Size.y;
		left = -sideRatio;
		right = sideRatio;
	}
	else
	{
		sideRatio = m_Size.y / m_Size.x;
		top = -sideRatio;
		bottom = sideRatio;
	}
	m_Metrics = glm::vec4(left, right, bottom, top);
	return m_Metrics;
}

void Renderer::drawObject(const BaseObject * bo, const glm::mat4 & viewMx)
{
	if (bo->isRemoved())
		return;

	GLuint bufferId = -1;
	GLuint indexBufferId = -1;

	// Looking in cache
	std::map<uint16_t, BufferInfo>::iterator it = m_ObjectCache.find(bo->crc());
	if(it == m_ObjectCache.end())
	{
		loadObject(bo, &bufferId, &indexBufferId);
		m_ObjectCache.insert(std::make_pair(bo->crc(), BufferInfo(m_CurrentTime, bufferId, indexBufferId)));
	}
	else
	{
		bufferId = (*it).second.bufferId;
		indexBufferId = (*it).second.indexBufferId;
	}

	// Preparing
	glm::mat4 modelMx;
	modelMx = glm::translate(modelMx, glm::vec3(bo->position(), 0.0f));
	modelMx = glm::rotate(modelMx, bo->angle(), glm::vec3(0.0f, 0.0f, 1.0f));

	const GLint progId = Manager::instance()->commonProgramId();
	glUseProgram(progId);
	glBindBuffer(GL_ARRAY_BUFFER, bufferId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);

	GLint posAttrId = glGetAttribLocation(progId, "a_Position");
	glVertexAttribPointer(posAttrId, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

	GLint colAttrId = glGetAttribLocation(progId, "a_Color");
	glVertexAttribPointer(colAttrId, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, r));

	GLuint viewMxUnifId = glGetUniformLocation(progId, "u_P");
	glUniformMatrix4fv(viewMxUnifId, 1, GL_FALSE, &viewMx[0][0]);

	GLuint modelMxUnifId = glGetUniformLocation(progId, "u_MV");
	glUniformMatrix4fv(modelMxUnifId, 1, GL_FALSE, &modelMx[0][0]);

	// Drawing
	glEnableVertexAttribArray(posAttrId);
	glEnableVertexAttribArray(colAttrId);
	glDrawElements(GL_TRIANGLE_STRIP, bo->indices().size(), GL_UNSIGNED_INT, 0);
	glDisableVertexAttribArray(colAttrId);
	glDisableVertexAttribArray(posAttrId);
}

void Renderer::loadObject(const BaseObject * bo, GLuint * bufferId, GLuint * indexBufferId)
{
	glGenBuffers(1, bufferId);
	glBindBuffer(GL_ARRAY_BUFFER, *bufferId);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * bo->vertices().size(), &bo->vertices()[0], GL_STATIC_DRAW);

	glGenBuffers(1, indexBufferId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *indexBufferId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * bo->indices().size(), &bo->indices()[0], GL_STATIC_DRAW);
}
