#include <string>
#include <OpenGLES/ES2/gl.h>

class Manager
{
public:
	static void init(const std::string & dataPath);
	static Manager * instance();

	inline GLint commonProgramId() const { return m_CommonProgramId; }
	inline GLuint projectileBuffId() const { return m_ProjectileBuffId; }
	inline GLuint projectileIndexBuffId() const { return m_ProjectileIndexBuffId; }

private:
	Manager(const std::string & dataPath);
	~Manager() throw();

	void getFileData(const std::string & fileName, char ** data, uint64_t * size);
	GLint compileShader(const std::string & fileName, GLenum shaderType);
	GLint createProgram(GLint vertexHandle, GLint fragmentHandle);

	void loadShaders();
	void createProjectileBuffer();

	std::string m_DataPath;
	GLint m_CommonProgramId; // Simplyfying work with program in this example code.
	GLuint m_ProjectileBuffId;
	GLuint m_ProjectileIndexBuffId;
};
