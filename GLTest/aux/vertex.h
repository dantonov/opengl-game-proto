
#ifndef __63414ea5b43a8ef81a2722c0e9b84fe6__
#define __63414ea5b43a8ef81a2722c0e9b84fe6__

#pragma pack(push, 1)

struct Vertex
{
	float x;
	float y;

	float r;
	float g;
	float b;

	inline Vertex(): x(0), y(0), r(1), g(1), b(1) {}
	inline Vertex(float x_, float y_, float r_, float g_, float b_)
		: x(x_), y(y_), r(r_), g(g_), b(b_) {}
};

#pragma pack(pop)

#endif
