#include "simple_math.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

bool EqualDoubles(double n1, double n2, double precision_)
{
  return (fabs(n1-n2) <= precision_);
}

int LineCircleIntersection(double x0, double y0, double r, // центр и рдиус окружности
                           double x1, double y1,           // точки
                           double x2, double y2,           //    отрезка
                           double& xa, double &ya,         // резуль-
                           double& xb, double &yb)         //      тат
{
	double q = x0*x0+y0*y0-r*r;
	double k = -2.0*x0;
	double l = -2.0*y0;

	double z = x1*y2-x2*y1;
	double p = y1-y2;
	double s = x1-x2;

	if (EqualDoubles(s, 0.0, 0.001))
	    s = 0.001;

	double A = s*s+p*p;
	double B = s*s*k+2.0*z*p+s*l*p;
	double C = q*s*s+z*z+s*l*z;

	double D = B*B-4.0*A*C;

	if (D < 0.0)
	    return 0;
  	else if (D < 0.001)
	{
		xa = -B/(2.0*A);
		ya = (p*xa + z)/s;
		return 1;
	}
	else
	{
		xa = (-B + sqrt(D))/(2.0*A);
		ya = (p*xa + z)/s;

		xb = (-B - sqrt(D))/(2.0*A);
		yb = (p*xb + z)/s;
	}
	return 2;
}

/* http://algolist.manual.ru/maths/geom/intersect/lineline2d.php. */
bool IsLinesCross(double x11, double y11, double x12, double y12, double x21, double y21, double x22, double y22)
{
	double maxx1 = std::max(x11, x12); double maxy1 = std::max(y11, y12);
	double minx1 = std::min(x11, x12); double miny1 = std::min(y11, y12);
	double maxx2 = std::max(x21, x22); double maxy2 = std::max(y21, y22);
	double minx2 = std::min(x21, x22); double miny2 = std::min(y21, y22);

	if (minx1 > maxx2 || maxx1 < minx2 || miny1 > maxy2 || maxy1 < miny2)
  		return false;  // Момент, когда линии имеют одну общую вершину...

	double dx1 = x12-x11; double dy1 = y12-y11; // Длина проекций первой линии на ось x и y
	double dx2 = x22-x21; double dy2 = y22-y21; // Длина проекций второй линии на ось x и y
	double dxx = x11-x21; double dyy = y11-y21;
	double div, mul;

	if ((div = ((double)dy2*dx1-(double)dx2*dy1)) == 0)
		return false; // Линии параллельны...
	if (div > 0)
	{
		if ((mul = ((double)dx1*dyy-(double)dy1*dxx)) < 0 || mul > div)
			return false; // Первый отрезок пересекается за своими границами...
		if ((mul = ((double)dx2*dyy-(double)dy2*dxx)) < 0 || mul > div)
			return false; // Второй отрезок пересекается за своими границами...
	}

	if ((mul = -((double)dx1*dyy-(double)dy1*dxx)) < 0 || mul > -div)
		return false; // Первый отрезок пересекается за своими границами...
	if ((mul = -((double)dx2*dyy-(double)dy2*dxx)) < 0 || mul > -div)
		return false; // Второй отрезок пересекается за своими границами...

	return true;
}

float length(float x, float y)
{
	return sqrt(x * x + y * y);
}

/** http://www.cyberforum.ru/cpp-beginners/thread261949.html */
bool triangleContainsPoint(float t1x, float t1y, float t2x, float t2y, float t3x,
	float t3y, float px, float py)
{
        double pl1, pl2, pl3;
        pl1 = (t1x - px)*(t2y - t1y)-(t2x - t1x)*(t1y - py);
        pl2 = (t2x - px)*(t3y - t2y)-(t3x - t2x)*(t2y - py);
        pl3 = (t3x - px)*(t1y - t3y)-(t1x - t3x)*(t3y - py);
        if ((pl1 >= 0 && pl2 >= 0 && pl3 >= 0) || (pl1 <= 0 && pl2 <= 0 && pl3 <= 0))
        {
            return true;
        }
        return false;
}

//http://stackoverflow.com/questions/10564491/function-to-calculate-a-crc16-checksum
#define CRC16 0x8005
uint16_t gen_crc16(const uint8_t *data, uint16_t size)
{
    uint16_t out = 0;
    int bits_read = 0, bit_flag;

    /* Sanity check: */
    if(data == NULL)
        return 0;

    while(size > 0)
    {
        bit_flag = out >> 15;

        /* Get next bit: */
        out <<= 1;
        out |= (*data >> bits_read) & 1; // item a) work from the least significant bits

        /* Increment bit counter: */
        bits_read++;
        if(bits_read > 7)
        {
            bits_read = 0;
            data++;
            size--;
        }

        /* Cycle check: */
        if(bit_flag)
            out ^= CRC16;

    }

    // item b) "push out" the last 16 bits
    int i;
    for (i = 0; i < 16; ++i) {
        bit_flag = out >> 15;
        out <<= 1;
        if(bit_flag)
            out ^= CRC16;
    }

    // item c) reverse the bits
    uint16_t crc = 0;
    i = 0x8000;
    int j = 0x0001;
    for (; i != 0; i >>=1, j <<= 1) {
        if (i & out) crc |= j;
    }

    return crc;
}

void rotateVector(float angle, std::vector<Vertex> & vertices)
{
	glm::mat4 rotateMx;
	rotateMx = glm::rotate(rotateMx, angle, glm::vec3(0.0f, 0.0f, 1.0f));

	for (std::vector<Vertex>::iterator it = vertices.begin(); it != vertices.end(); ++it)
	{
		glm::vec4 vec(it->x, it->y, 0, 1);
		vec = vec * rotateMx;
		it->x = vec.x;
		it->y = vec.y;
	}
}
