#ifndef __a697bb910644c61b2d8fd9b5795f7f72__
#define __a697bb910644c61b2d8fd9b5795f7f72__

#include <math.h>
#include <algorithm>
#include <vector>
#include "vertex.h"

/** http://forum.codenet.ru/q59244/%D0%9F%D0%B5%D1%80%D0%B5%D1%81%D0%B5%D1%87%D0%B5%D0%BD%D0%B8%D0%B5+%D0%BE%D1%82%D1%80%D0%B5%D0%B7%D0%BA%D0%B0+%D0%B8+%D0%BE%D0%BA%D1%80%D1%83%D0%B6%D0%BD%D0%BE%D1%81%D1%82%D0%B8?s=0#answer_376404 */

bool EqualDoubles(double n1, double n2, double precision_);
int LineCircleIntersection(double x0, double y0, double r, // центр и рдиус окружности
                           double x1, double y1,           // точки
                           double x2, double y2,           //    отрезка
                           double& xa, double &ya,         // резуль-
                           double& xb, double &yb);         //      тат

/* http://algolist.manual.ru/maths/geom/intersect/lineline2d.php. */
bool IsLinesCross(double x11, double y11, double x12, double y12, double x21, double y21, double x22, double y22);
float length(float x, float y);

template <typename T>
float length(T t)
{
	return sqrt(t.x * t.x + t.y * t.y);
}

bool triangleContainsPoint(float t1x, float t1y, float t2x, float t2y, float t3x,
	float t3y, float px, float py);

uint16_t gen_crc16(const uint8_t *data, uint16_t size);

void rotateVector(float angle, std::vector<Vertex> & vertices);

#endif
