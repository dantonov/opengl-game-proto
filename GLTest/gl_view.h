#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <QuartzCore/QuartzCore.h>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>

@interface GLView : GLKView
{
	CAEAGLLayer* m_GLLayer;
	EAGLContext* m_GLContext;
	GLuint m_ColorRenderBuffer;
	CFTimeInterval prevTime;
	float checkCacheTime;
}

@property (nonatomic, assign, readonly) CADisplayLink * displayLink;
@property (nonatomic) BOOL paused;
@property (nonatomic) bool startFrame;

+ (Class)layerClass;

- (void)setupLayer;
- (void)setupContext;
- (void)setupRenderBuffer;
- (void)setupFrameBuffer;

@end
